# README #

Technical Test for The Floow.  
Creation Time: ~10 hours.

### Requirements ###
Ruby version 2.1.5  
Rails version 4.2  
Postgres Database  

### Installation Notes ###
Please ensure you have phantomJS installed for feature testing  
Database details in database.yml require changing  

###Project Notes###

Bootstrap has been used in conjunction with JQuery so JavaScript needs to be enabled.  
Tested on the following browsers: Firefox, Chrome, IE12, MS Edge.  

Heroku WebApp location: [http://aqueous-plains-2865.herokuapp.com/](http://aqueous-plains-2865.herokuapp.com/)