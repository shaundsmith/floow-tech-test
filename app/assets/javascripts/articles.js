Floow.Articles = (function(){

    function processArticle(){
        var article = $('#article-contents').html();
        article = article.replace(/<span class="bold-format">/g, '[b]');
        article = article.replace(/<span class="italic-format">/g, '[i]');

        article = article.replace(/<\/span>/g, '[/f]');

        article = article.replace(/<h2>/g, '[t]');
        article = article.replace(/<\/h2>/g, '[/t]');

        article = article.replace(/<div class="code">/g, '[code]');
        article = article.replace(/<\/div>/g, '[/code]');
        article = article.replace(/<h3 class="code-title">/g, '[ct]');
        article = article.replace(/<\/h3>/g, '[/ct]');

        article = article.replace(/<br>/g, '[lb]');

        return article;
    }

    function processPlainText(){
        var article = $('#article-contents').html();
        article = article.replace(/<span class="bold-format">/g, '');
        article = article.replace(/<span class="italic-format">/g, '');

        article = article.replace(/<\/span>/g, '');

        article = article.replace(/<h2>/g, '');
        article = article.replace(/<\/h2>/g, '');

        article = article.replace(/<div class="code">/g, '');
        article = article.replace(/<\/div>/g, '');
        article = article.replace(/<h3 class="code-title">/g, '');
        article = article.replace(/<\/h3>/g, '');

        article = article.replace(/<br>/g, '');
        article = article.replace(/&nbsp;/g, '');

        return article;
    }

    return{
        editArticle : function(){
            if(!$('#article').hasClass("editable")){
                $("#article").addClass("editable");
                $("#article-contents").prop('contenteditable', true);
                $("#article-title").prop('contenteditable', true);
                $('#edit-button').addClass("hidden");
                $('#save-button').removeClass("hidden");
                $('#wysiwyg').removeClass("hidden");
            }
        },

        updateArticle : function(){
            $.ajax({
                url : $("#article-edit-url").val(),
                type : "PUT",
                data : {"title" : $('#article-title').html(), "plain_text" : processPlainText(), "formatted_text" : processArticle()}
            }).done(function(message){
                $("#article").removeClass("editable");
                $("#article-contents").prop('contenteditable', false);
                $("#article-title").prop('contenteditable', false);
                $('#save-button').addClass("hidden");
                $('#edit-button').removeClass("hidden");
                $('#wysiwyg').addClass("hidden");
            }).fail(function(message){
                $(".flash-messages").html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert">×</a>'+message.responseText+'</a></div>');
            })

        },

        newArticle : function(){
            $('#plain_text').val(processPlainText());
            $('#formatted_text').val(processArticle());
            $('#title').val($('#article-title').html());
            $('#new-form')[0].submit();
        },

        addCode : function(){
            var codeDiv = '<div class="code"><h3 class="code-title">Insert Title Here</h3>Insert Code Here</div>';
            $("#article-contents").html($("#article-contents").html() + codeDiv);
        },

        addBold : function(){
            $("#article-contents").html($("#article-contents").html() + '<span class="bold-format">Insert Bold Text Here</span>&nbsp;');
        },

        addItalic : function(){
            $("#article-contents").html($("#article-contents").html() + '<span class="italic-format">Insert Italic Text Here</span>&nbsp;');
        },

        addHeader : function(){
            $("#article-contents").html($("#article-contents").html() + '<h2>Insert Header Here</h2>&nbsp;');
        }
    };

})();