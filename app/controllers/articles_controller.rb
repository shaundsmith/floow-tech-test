class ArticlesController < ApplicationController
  load_and_authorize_resource
  before_action :set_article, :only => [:show, :edit, :update, :destroy]
  helper FormatArticleHelper

  def index
    if params[:search].blank?
      @articles = Article.paginate(:page => params[:page])
    else
      @articles = Article.where("lower(title) LIKE ?", "%#{params[:search].downcase}%").paginate(:page => params[:page])
    end
    @index_articles= true
  end

  def show
    @new = false
  end

  def new
    @article = Article.new({
      :title => "Insert Title Here",
      :formatted_text => "Insert Article Contents Here"
    })
    @new = true
    render "articles/show"
  end

  def create
    @article = Article.new({
      :title => params[:title].strip,
      :plain_text => params[:plain_text].strip,
      :formatted_text => params[:formatted_text].strip,
      :author => current_user
      })
    if @article.save
      flash[:notice] = "Article Created Successfully"
      redirect_to article_path(:id => @article.id)
    else
      @new = true
      flash[:error] = @article.errors.full_messages
      render "articles/show"
    end
  end

  def update
    article = Article.find(params[:id])
    article.assign_attributes({
      :title => params[:title].strip,
      :plain_text => params[:plain_text].strip,
      :formatted_text => params[:formatted_text].strip
      })
    if article.save
      render :plain => "Article Updated Successfully", :status => 200
    else
      render :plain => "Article Update Failed : #{article.errors.full_messages}", :status => 503
    end
  end

  private
    def set_article
      @article = Article.find(params[:id])
    end

end