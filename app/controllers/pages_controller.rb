class PagesController < ApplicationController

  def index
    @articles = Article.order(:created_at).limit(5)
  end

end