module ApplicationHelper
  def format_date(date)
    unless date.nil?
      return date.strftime("%d/%m/%y")
    end
    return "-"
  end

  def text_limit(text, limit=100)
    if text.length > limit
      return text[0,97] + "..."
    else
      return text
    end
  end
end
