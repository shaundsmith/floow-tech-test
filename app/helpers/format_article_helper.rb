module FormatArticleHelper
	def format_article(article)
		output_html = article.formatted_text
		unless output_html.blank?
			#Ensuring that the article formatting is the only executable HTML
			output_html.gsub!('<', '&lt;')
			output_html.gsub!('>', '&gt;')
			output_html.gsub!('[lb]', '<br>');
			#Series of string replacements for formatting
			#[b][/f] represents bold text
			output_html.gsub!('[b]', '<span class="bold-format">')
			output_html.gsub!('[/f]', "</span>")
			#[i][/f] represents italics text
			output_html.gsub!('[i]', '<span class="italic-format">')
			output_html.gsub!('[/f]', '</span>')
			#[t][/t] represents subtitle
			output_html.gsub!('[t]', '<h2>')
			output_html.gsub!('[/t]', '</h2>')
			#[code][/code] represents code		
			output_html.gsub!('[code]', '<div class="code">')
			output_html.gsub!('[/code]', '</div>')
			output_html.gsub!('[ct]', '<h3 class="code-title">')
			output_html.gsub!('[/ct]', '</h3>')
			return output_html.html_safe
		else
			return output_html
		end
	end
end