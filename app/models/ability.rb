class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, Article
    unless user.blank?
        can :create, Article
        can [:update, :destroy], Article do |a|
            a.author == user
        end
    end
  end
end
