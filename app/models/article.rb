class Article < ActiveRecord::Base

  default_scope { order(updated_at: :desc) }

  belongs_to :author, :class_name => "User"
  self.per_page = 20

  validates :title, :presence => {:message => "Please enter a title"}, :length => {:maximum => 40, :minimum => 3, :too_long => "Title can only be 40 characters long", :too_short => "Title must be 3 or more characters"}
  validates_presence_of :author_id, :message => "Please provide an Author"
  validates :formatted_text, :presence => {:message => "Please provide article content"}, :length => {:maximum => 2500, :minimum => 3, :too_long => "Article can only be 2500 characters long", :too_short => "Article must be 3 or more characters"}
  validates :plain_text, :presence => {:message => "Please provide article content"}, :length => {:maximum => 2500, :minimum => 3, :too_long => "Article can only be 2500 characters long", :too_short => "Article must be 3 or more characters"}

end
