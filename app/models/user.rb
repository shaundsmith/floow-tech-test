class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :articles, :class_name => "Article", :foreign_key => 'author_id'

  validates :first_name, :length => {:minimum => 2, :maximum => 25, :too_long => "First name must be 25 characters or less", :too_short => "First name must be 2 characters or more"}
  validates :last_name, :length => {:minimum => 2, :maximum => 25, :too_long => "Last name must be 25 characters or less", :too_short => "Last name must be 2 characters or more"}


  def name
    return self.first_name + " " + self.last_name
  end

end
