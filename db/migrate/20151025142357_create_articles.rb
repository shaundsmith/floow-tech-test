class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.integer :author_id
      t.text :plain_text
      t.text :formatted_text

      t.timestamps null: false
    end
  end
end
