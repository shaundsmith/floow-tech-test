require 'faker'
require 'factory_girl'

FactoryGirl.define do
  matching_text = Faker::Lorem.paragraph(5)
  factory :article do
    title {Faker::Lorem.characters(40)}
    plain_text { matching_text }
    formatted_text { "[b]" + matching_text + "[/f]" }
    author_id {Faker::Number.digit}
  end
end