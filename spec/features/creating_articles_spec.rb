require 'rails_helper'
require 'spec_helper'

describe 'creating articles' do
  before(:each) do
    @user = FactoryGirl.create(:user)
  end

  it 'shouldn\'t allow unauthorised users to create articles' do
    ability = Ability.new(nil)
    assert ability.cannot?(:create, Article.new)
  end

  it 'should allow authorised users to create articles' do
    login_as(@user)
    visit new_article_path
    expect(page).to have_text("Insert Title Here")
  end

  it 'should allow authorised users to save articles', :js => true do
    login_as(@user)
    visit new_article_path
    page.find("#new-button").click
    expect(page).to have_text("Insert Title Here")
    expect(page).to_not have_text("Save")
  end

  it 'should allow users to save articles with bold formatting', :js => true do
    login_as(@user)
    visit new_article_path
    page.find("#add-bold").click
    page.find("#new-button").click
    expect(page).to have_css(".bold-format")
    expect(page).to_not have_text("Save")
  end

  it 'should allow users to save articles with italic formatting', :js => true do
    login_as(@user)
    visit new_article_path
    page.find("#add-italic").click
    page.find("#new-button").click
    expect(page).to have_css(".italic-format")
    expect(page).to_not have_text("Save")
  end

  it 'should allow users to save articles with header formatting', :js => true do
    login_as(@user)
    visit new_article_path
    page.find("#add-header").click
    page.find("#new-button").click
    expect(page).to have_text("Insert Header Here")
    expect(page).to_not have_text("Save")
  end

  it 'should allow users to save articles with code sections', :js => true do
    login_as(@user)
    visit new_article_path
    page.find("#add-code").click
    page.find("#new-button").click
    expect(page).to have_css(".code")
    expect(page).to_not have_text("Save")
  end

  it 'should display errors if invalid data is presented', :js => true do
    login_as(@user)
    visit new_article_path
    page.execute_script("$('#article-title').html('');")
    save_and_open_page
    page.find("#new-button").click
    expect(page).to have_text("Save")
    expect(page).to have_text("Title must be 3 or more characters")
  end

end