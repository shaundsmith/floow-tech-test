require 'rails_helper'
require 'spec_helper'

describe 'updating articles' do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @article = FactoryGirl.create(:article, :author => @user)
  end

  it 'shouldn\'t allow unauthorised users to update articles' do
    ability = Ability.new(nil)
    assert ability.cannot?(:update, Article.new)
  end

  it 'shouldn\'t allow authorised users to update articles who are not the author' do
    ability = Ability.new(FactoryGirl.create(:user))
    assert ability.cannot?(:update, Article.new)
  end

  it 'should allow authorised users to update articles' do
    login_as(@user)
    visit article_path(@article)
    expect(page).to have_text(@article.title)
    expect(page).to have_text("Edit")
  end

  it 'should allow authorised users to save article updates', :js => true do
    login_as(@user)
    visit article_path(@article)
    page.find("#edit-button").click
    page.execute_script("$('#article-title').html('New Title');")
    page.find("#save-button").click
    expect(page).to have_text("Article Updated Successfully")
    expect(page).to have_text("New Title")

  end

  it 'should display errors if invalid data is presented', :js => true do
    login_as(@user)
    visit article_path(@article)
    page.find("#edit-button").click
    page.execute_script("$('#article-title').html('');")
    page.find("#save-button").click
    expect(page).to have_text("Article Update Failed")
  end

end