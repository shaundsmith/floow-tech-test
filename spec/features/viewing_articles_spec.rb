require 'rails_helper'
require 'spec_helper'

describe 'viewing articles' do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @article = FactoryGirl.create(:article, :author => @user)
    @user_2 = FactoryGirl.create(:user)
  end

  it 'should show a list of the latest articles' do
    visit root_path
    expect(page).to have_text(@article.title)
  end

  it 'should show a list of articles on the article directory' do
    visit articles_path
    expect(page).to have_text(@article.title)
  end

  it 'should search for an article on the article directory' do
    article_2 = FactoryGirl.create(:article, :author => @user)
    visit articles_path
    expect(page).to have_text(@article.title)
    expect(page).to have_text(article_2.title)
    page.find("#search").set(article_2.title)
    page.find("#search-btn").click
    expect(page).to have_text(article_2.title)
    expect(page).to_not have_text(@article.title)
  end

  it 'should allow a user to see another\'s articles' do
    article = FactoryGirl.create(:article, :author => @user)
    article_2 = FactoryGirl.create(:article, :author => @user_2)
    visit user_path(@user_2.id)
    expect(page).to have_text(article_2.title)
    visit user_path(@user.id)
    expect(page).to have_text(article.title)
  end

  it 'should allow a user to view an article' do
    visit article_path(@article.id)
    expect(page).to have_text(@article.title)
    expect(page).to have_text(@article.author.name)
  end


end