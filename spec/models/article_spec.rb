require 'rails_helper'

RSpec.describe Article, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.create(:article)).to be_valid
  end

  it 'is invalid without a title' do
    expect(FactoryGirl.build(:article, :title => "")).to_not be_valid
  end

  it 'is invalid without plain text' do
    expect(FactoryGirl.build(:article, :plain_text => "")).to_not be_valid
  end

  it 'is invalid without formatted text' do
    expect(FactoryGirl.build(:article, :formatted_text => "")).to_not be_valid
  end

  it 'is invalid without an author' do
    expect(FactoryGirl.build(:article, :author_id => nil)).to_not be_valid
  end

  #Out of bounds

  it 'is invalid with a title < 3 characters' do
    expect(FactoryGirl.build(:article, :title => Faker::Lorem.characters(2))).to_not be_valid
  end

  it 'is invalid with a title > 40 characters' do
    expect(FactoryGirl.build(:article, :title => Faker::Lorem.characters(41))).to_not be_valid
  end

  #Boundary Testing
  it 'is valid with a title = 3 characters' do
    expect(FactoryGirl.build(:article, :title => Faker::Lorem.characters(3))).to be_valid
  end

  it 'is valid with a title = 40 characters' do
    expect(FactoryGirl.build(:article, :title => Faker::Lorem.characters(40))).to be_valid
  end

  #Out of bounds

  it 'is invalid with content < 3 characters' do
    content = Faker::Lorem.characters(2)
    expect(FactoryGirl.build(:article, :plain_text => content, :formatted_text => content)).to_not be_valid
  end

  it 'is invalid with content > 2500 characters' do
    content = Faker::Lorem.characters(2501)
    expect(FactoryGirl.build(:article, :plain_text => content, :formatted_text => content)).to_not be_valid
  end

  #Boundary Testing
  it 'is valid with content = 3 characters' do
    content = Faker::Lorem.characters(3)
    expect(FactoryGirl.build(:article, :plain_text => content, :formatted_text => content)).to be_valid
  end

  it 'is valid with a content = 2500 characters' do
    content = Faker::Lorem.characters(2500)
    expect(FactoryGirl.build(:article, :plain_text => content, :formatted_text => content)).to be_valid
  end

end
