require 'rails_helper'

RSpec.describe User, type: :model do
  it 'should have a valid factory' do
    expect(FactoryGirl.build(:user)).to be_valid
  end

  it 'should not be valid without a first name' do
    expect(FactoryGirl.build(:user, :first_name => "")).to_not be_valid
  end 

  it 'should not be valid without a last name' do
    expect(FactoryGirl.build(:user, :last_name => "")).to_not be_valid
  end

  it 'should not be valid without an email' do
    expect(FactoryGirl.build(:user, :email => "")).to_not be_valid
  end

  it 'should not be valid with a last name > 25 characters' do
    expect(FactoryGirl.build(:user, :last_name => Faker::Lorem.characters(26))).to_not be_valid
  end

  it 'should not be valid with a last name < 3 characters' do
    expect(FactoryGirl.build(:user, :last_name => Faker::Lorem.characters(2))).to_not be_valid
  end 

  it 'should be valid with a last name = 25 characters' do
    expect(FactoryGirl.build(:user, :last_name => Faker::Lorem.characters(25))).to be_valid
  end 

  it 'should be valid with a last name = 3 characters' do
    expect(FactoryGirl.build(:user, :last_name => Faker::Lorem.characters(3))).to be_valid
  end

  it 'should not be valid with a first name > 25 characters' do
    expect(FactoryGirl.build(:user, :first_name => Faker::Lorem.characters(26))).to_not be_valid
  end

  it 'should not be valid with a first name < 3 characters' do
    expect(FactoryGirl.build(:user, :first_name => Faker::Lorem.characters(2))).to_not be_valid
  end 

  it 'should be valid with a first name = 25 characters' do
    expect(FactoryGirl.build(:user, :first_name => Faker::Lorem.characters(25))).to be_valid
  end 

  it 'should be valid with a first name = 3 characters' do
    expect(FactoryGirl.build(:user, :first_name => Faker::Lorem.characters(3))).to be_valid
  end 

  it 'should successfully append first name and last name' do
    user = FactoryGirl.build(:user, :first_name => "Bob", :last_name => "Smith")
    expect(user.name).to eq("Bob Smith")
  end

end
